# astra README

This is a syntax highlighting plugin for the ASTRA agent programming language: http://www.astralanguage.com/

## Features

It provides syntax highlighting

## Requirements

ASTRA is built on Java and uses maven


## Release Notes

Users appreciate release notes as you update your extension.

### 0.0.1

Initial release of the plugin

### 0.0.2

Fixed issue #1 and #2
