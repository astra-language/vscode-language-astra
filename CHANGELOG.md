# Change Log

All notable changes to the "astra" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.0.2] - 2020-02-07
### Added
- Missing syntax highlighting for at_index and list_count terms
- Syntax Highlighting for comments

